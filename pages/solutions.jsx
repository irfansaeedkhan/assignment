// components
import HeaderComponent from "@/components/solutions/HeaderComponent";
import Section1 from "@/components/solutions/Section1";
import Section2 from "@/components/solutions/Section2";
import Section3 from "@/components/solutions/Section3";
import Section4 from "@/components/solutions/Section4";

export default function Solutions() {
  return (
    <div className="mainPageContainer">
      <HeaderComponent />
      <Section1 />
      <Section2 />
      <Section3 />
      <Section4 />
    </div>
  );
}
