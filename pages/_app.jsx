import React from "react";
import Layout from "@/components/global/Layout";
import { useRouter } from "next/router";
import Head from "next/head";
import "../styles/index.scss";


function MyApp({ Component, pageProps }) {
  const Paths = ["", "/", "/solutions", "/news", "/careers", "/locations"];
  const router = useRouter();
  let showHeaderFooter = Paths.includes(router.pathname);
  return (
    <React.Fragment>
      <Head>
        <title>Assignment</title>
      </Head>
      {showHeaderFooter && (
        <Layout>
          <Component {...pageProps} />
        </Layout>
      )}
      {!showHeaderFooter && (
        <div className="wrongUrl" style={{display : 'flex', justifyContent : 'center', alignItems : 'center', height: '100vh'}}>
          <div className="content" style={{ height: '20vh', textAlign: 'center'}}>
            <h1 style={{ fontSize: '50px'}}>404</h1>
            <p style={{ fontSize: '20px'}}>Wrong Link</p>
            <div className="backBtn" onClick={() => router.back()}>
              <span className="label">Back</span>
          </div>
          </div>
        </div>
      )}
    </React.Fragment>
  );
}

export default MyApp;
