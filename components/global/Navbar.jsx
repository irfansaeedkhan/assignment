import React from "react";
import CustomButton from "@/components/global/CustomButton";
import Link from "next/link";
import { useRouter } from "next/router";
const NavbarToggle = () => {
  const hamburger = document.querySelector(".hamburger");
  const navLinks = document.querySelector(".nav-linksMobile");
  // const links = document.querySelectorAll(".nav-linksMobile li");
  const layoutContainer = document.getElementById("layoutContainer");
  const customnav = document.querySelector(".customnav");
  const navContainer = document.querySelector(".navContainer");
  navLinks.classList.toggle("open");
  navContainer.classList.toggle("open");
  layoutContainer.classList.toggle("open");
  customnav.classList.toggle("open");
  // links.forEach((link) => {
  //   link.classList.toggle("fade");
  // });

  //Hamburger Animation
  hamburger.classList.toggle("toggle");
};

function Navbar() {
  const router = useRouter();

  return (
    <div className="navContainer">
      <nav className="customnav">
        <div className="navInner">
          <div className="logo desktop">
            <Link href="/">
            <div className="logoContainer">
                <p>L</p>
                <p>O</p>
                <p>G</p>
                <p>O</p>
                </div>
            </Link>
          </div>
          <div className="logo mobile">
            <Link href="/">
            <div className="logoContainer">
                <p>L</p>
                <p>O</p>
                <p>G</p>
                <p>O</p>
                </div>
            </Link>
          </div>
          <div className="hamburger" onClick={() => NavbarToggle()}>
            <div className="line1"></div>
            <div className="line2"></div>
            <div className="line3"></div>
          </div>
          <ul className="nav-links">
            <li className={router.pathname == "/" ? "active" : ""}>
              <Link href="/">The Firm</Link>
            </li>
            <li className={router.pathname == "/solutions" ? "active" : ""}>
              <Link href="/solutions">Solutions</Link>
            </li>
            <li className={router.pathname == "/news" ? "active" : ""}>
              <Link href="/news">News & insights</Link>
            </li>
            <li className={router.pathname == "/careers" ? "active" : ""}>
              <Link href="/careers">Careers</Link>
            </li>
            <li className={router.pathname == "/locations" ? "active" : ""}>
              <Link href="/locations">Locations</Link>
            </li>
          </ul>
          <CustomButton
            border="none"
            backgroundColor="#012A38"
            text="Speak To An Expert"
            maxWidth="18.1rem"
            padding={"0.8rem 0"}
          />
        </div>
        <div className="mobilenavContainer">
          <ul className="nav-linksMobile">
            <li
              className={router.pathname == "/" ? "active" : ""}
              onClick={() => NavbarToggle()}
            >
              <Link href="/">The Firm</Link>
            </li>
            <li
              className={router.pathname == "/solutions" ? "active" : ""}
              onClick={() => NavbarToggle()}
            >
              <Link href="/solutions">Solutions</Link>
            </li>
            <li
              className={router.pathname == "/news" ? "active" : ""}
              onClick={() => NavbarToggle()}
            >
              <Link href="/news">News & insights</Link>
            </li>
            <li
              className={router.pathname == "/careers" ? "active" : ""}
              onClick={() => NavbarToggle()}
            >
              <Link href="/careers">Careers</Link>
            </li>
            <li
              className={router.pathname == "/locations" ? "active" : ""}
              onClick={() => NavbarToggle()}
            >
              <Link href="/locations">Locations</Link>
            </li>
            <li>
              <CustomButton
                border="none"
                backgroundColor="#012A38"
                text="Speak To An Expert"
                maxWidth="18.1rem"
                padding={"0.8rem 1.1rem"}
              />
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
}

export default Navbar;
