import React, { useRef, useEffect, useLayoutEffect } from "react";
import { gsap } from "gsap";
import ScrollTrigger from "gsap/dist/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);

function CustomAnimation() {
  useEffect(() => {
    // repeated logo animation
    const logoRepeatAnimation = gsap
      .timeline({ repeat: -1, repeatDelay: 5 })
      .fromTo(
        ".logo .logoContainer p",
        {
          scale: 0,
        },
        {
          duration: 0.4,
          scale: 1,
          stagger: 0.2,
          ease: "elastic.out(1, 0.5)",
        }
      );
    let logo = document.querySelector(".logo .logoContainer");
    logo.addEventListener("mouseover", function () {
      logoRepeatAnimation.play();
    });
    logo.addEventListener("mouseleave", function () {
      logoRepeatAnimation.play(0);
    });

    // intro Animation
    function introAnimation() {
      const animation = gsap
        .timeline()
        .set(".logoletters h6", { scale: 0 })
        .set(".dotpulse", { x: 130, y: 68 })
        .fromTo(
          ".dotpulse",
          { opacity: 1 },
          { opacity: 0.2, repeat: 3, yoyo: true, duration: 0.4 }
        )
        .fromTo(
          "#downLine",
          { scaleX: 0, x: 0, y: -53 },
          { scaleX: 1, x: 0, y: -53, duration: 1, ease: "power4.easeOut" }
        )
        .fromTo(
          "#upLine",
          { scaleX: 0, x: 0, y: 52 },
          { scaleX: 1, x: 0, y: 52, duration: 1, ease: "power4.easeOut" },
          "<"
        )
        .to(".dotpulse", { opacity: 0, duration: 0.3 })
        .fromTo("#upLine", { y: 55 }, { y: 0, duration: 1 })
        .fromTo("#downLine", { y: -55 }, { y: 0, duration: 1 }, "<")
        .to(".logoletters h6", {
          duration: 0.4,
          scale: 1,
          stagger: 0.2,
          ease: "elastic.out(1, 0.5)",
        })
        .to(".introContainer", { opacity: 0, duration: 0.5, display: "none" });
    }
    // header animation
    function headerAnimation() {
      const tl = gsap
        .timeline()
        .fromTo(
          "header .headerInner",
          { backgroundSize: "55%" },
          { backgroundSize: "51%", duration: 1.5 }
        )
        .fromTo(
          ".headerBox .breadCrumbs",
          { autoAlpha: 0, y: -200 },
          { autoAlpha: 1, y: 0, duration: 1 },
          "<"
        )
        .fromTo(
          ".headerBox h1 p",
          { autoAlpha: 0, y: -200 },
          {
            autoAlpha: 1,
            y: 0,
            stagger: 0.1,
            ease: "elastic.out(1, 0.5)",
            duration: 1,
          }
        )
        .fromTo(
          ".headerBox .subTitle",
          { autoAlpha: 0, x: -200 },
          { autoAlpha: 1, x: 0, duration: 1 },
          "-=0.4"
        );
    }
    // scroll triger animation1
    const animateSection1 = () => {
      const tl = gsap
        .timeline({
          scrollTrigger: {
            trigger: "#layoutContainer",
            start: "top top",
            end: "5% top",
            scrub: 1.2,
            toggleActions: "restart complete reverse reset",
          },
        })
        .fromTo(
          [".section1Inner .txtBox h2", ".section1Inner .txtBox h4"],
          { autoAlpha: 0, x: -200 },
          { autoAlpha: 1, x: 0, duration: 1 }
        )
        .fromTo(
          ".section1Inner .imgBox",
          { autoAlpha: 0, top: 200 },
          { autoAlpha: 1, top: 20, duration: 1 },
          "<"
        )
        .fromTo(
          ".section1Inner .CustomButton",
          { autoAlpha: 0, x: -100 },
          { autoAlpha: 1, x: 0, ease: "elastic.out(1, 0.7)", duration: 0.4 }
        );
    };
    // scroll triger animationdual
    const contentAnimation = (secNum) => {
      const animateSection2 = gsap
        .timeline({
          // scrollTrigger: {
          //   trigger: ".section2",
          //   start: "top top", // when the top of the trigger hits the top of the viewport
          //   end: "+=500", // end after scrolling 500px beyond the start
          //   scrub: 1,
          //   toggleActions: "restart complete reverse reset",
          // }
          scrollTrigger: `.section${secNum}`, // start the animation when ".box" enters the viewport (once)
          x: 500,
          markers: true,
        })
        .fromTo(
          `.section${secNum} .sectionInner .content:nth-child(1) .imgBox`,
          {
            scale: 0.8,
            autoAlpha: 0,
            x: -300,
            rotate: -25,
            transformOrigin: "bottom left",
          },
          {
            scale: 1,
            autoAlpha: 1,
            x: 0,
            rotate: 0,
            transformOrigin: "bottom left",
            duration: 1.5,
          },
          1
        )
        .fromTo(
          [
            `.section${secNum} .content:nth-child(1) .txtBox h2`,
            `.section${secNum} .content:nth-child(1) .txtBox h4`,
          ],
          { autoAlpha: 0, x: 200 },
          { autoAlpha: 1, x: 0, duration: 1.5 },
          "<"
        )
        .fromTo(
          `.section${secNum} .content:nth-child(1) .CustomButton`,
          { autoAlpha: 0, x: 100 },
          { autoAlpha: 1, x: 0, ease: "elastic.out(1, 0.7)", duration: 0.4 }
        )
        .fromTo(
          `.section${secNum} .sectionInner .content:nth-child(2) .imgBox`,
          {
            scale: 0.9,
            autoAlpha: 0,
            x: 300,
            rotate: 25,
            transformOrigin: "bottom right",
          },
          {
            scale: 1,
            autoAlpha: 1,
            x: 0,
            rotate: 0,
            transformOrigin: "bottom right",
            duration: 1.5,
          },
          2.5
        )
        .fromTo(
          [
            `.section${secNum} .content:nth-child(2) .txtBox h2`,
            `.section${secNum} .content:nth-child(2) .txtBox h4`,
          ],
          { autoAlpha: 0, x: -200 },
          { autoAlpha: 1, x: 0, duration: 1.5 },
          "<"
        )
        .fromTo(
          `.section${secNum} .content:nth-child(2) .CustomButton`,
          { autoAlpha: 0, x: -100 },
          { autoAlpha: 1, x: 0, ease: "elastic.out(1, 0.7)", duration: 0.4 }
        );
    };



    function installMediaQueryWatcher(mediaQuery, layoutChangedCallback) {
      var mql = window.matchMedia(mediaQuery);
      mql.addListener(function (e) { return layoutChangedCallback(e.matches); });
      layoutChangedCallback(mql.matches);
    }

    installMediaQueryWatcher("(min-width: 1200px)", function(matches) {
  
      if (matches) {
        
        introAnimation();
        setTimeout(headerAnimation, 5000);
        animateSection1();
        contentAnimation(2);
        contentAnimation(3);
        contentAnimation(4);
        
      } else {console.log("gsap for mobile not ready")}
    });
  });

  return (
    <div className="introContainer">
      <div className="introInner">
        <div className="area">
          <ul className="circles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
          </ul>
        </div>
        <div className="content">
          <div className="animateLogo">
            <div className="dotpulse"></div>
            <div id="upLine"></div>
            <div className="logoletters">
              <h6>L</h6>
              <h6>O</h6>
              <h6>G</h6>
              <h6>O</h6>
            </div>
            <div id="downLine"></div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CustomAnimation;
