import Image from "next/image";
import Link from "next/link";
import CustomButton from "@/components/global/CustomButton";
import { useRouter } from "next/router";

function Footer() {
  const router = useRouter();
  const handleContactForm = () => {
    alert("handle");
  };
  return (
    <footer>
      <div className="footerInner">
        <div className="footerLevel1">
          <div className="footerlogoContainer">
            <Link href="/">
              <div className="logoContainer">
                <p>L</p>
                <p>O</p>
                <p>G</p>
                <p>O</p>
                </div>
            </Link>
          </div>
          <div className="footerMenuList">
            <ul className="footernav-links">
              <li className={router.pathname == "/" ? "active" : ""}>
                <Link href="/">The Firm</Link>
              </li>
              <li className={router.pathname == "/solutions" ? "active" : ""}>
                <Link href="/solutions">Solutions</Link>
              </li>
              <li className={router.pathname == "/news" ? "active" : ""}>
                <Link href="/news">News & insights</Link>
              </li>
              <li className={router.pathname == "/careers" ? "active" : ""}>
                <Link href="/careers">Careers</Link>
              </li>
              <li className={router.pathname == "/locations" ? "active" : ""}>
                <Link href="/locations">Locations</Link>
              </li>
              <li className="Svgneon-button">
                <Link href="https://www.linkedin.com/in/irfansaeedkhan/">
                  <Image
                    layout="responsive"
                    objectFit="contain"
                    width={27}
                    height={27}
                    src="/assets/images/linkedInIcon.png"
                    alt="telegram"
                  />
                </Link>
              </li>
            </ul>
          </div>
          <div className="knowMoreContainer">
            <div className="knowMoreContent">
              <h3>What To Know More?</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nisl,
                diam lectus sagittis
              </p>
              <CustomButton
                border="none"
                backgroundColor="#012A38"
                onClick={handleContactForm}
                text="Speak To An Expert"
                maxWidth="18.1rem"
                padding={"0.8rem 0"}
              />
            </div>
          </div>
        </div>
        <div className="footerLevel2">
          <p>
            Wethaq (Capital Markets) Ltd is a limited liability company
            registered with the Dubai International Financial Centre (DIFC) with
            Company Registration No. 3254 and regulated by the Dubai Financial
            Services Authority (DFSA) under DFSA Reference Number F005322.
            Wethaq Capital Platform JSC operates under an Experimental Permit
            from the Capital Market Authority (CMA). Wethaq Solutions Ltd is a
            limited liability company registered in the United Kingdom.
          </p>
        </div>
        <div className="footerLevel3">
          <div className="copyrightContainer">
            <p>&copy; 2022 Wethaq. All rights reserved.</p>
          </div>
          <div className="linksContainer">
            <Link href="/">
              <p>Terms &amp; Conditions</p>
            </Link>
            <div className="verticalLine"></div>
            <Link href="/">
              <p>Privacy Policy</p>
            </Link>
          </div>
          <div className="createrContainer">
            <p>Made with &hearts; by TenTwenty</p>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
