import React from "react";
import Head from "next/head";
import Navbar from "./Navbar";
import Footer from "./Footer";
import { useRouter } from "next/router";
import CustomAnimation from "./CustomAnimation";
const Layout = ({ children }) => {
  const Paths = ["", "/", "/solutions", "/news", "/careers", "/locations"];
  const router = useRouter();
  let showHeaderFooter = Paths.includes(router.pathname);

  return (
    <>
      <Head>
        <title>Assignment</title>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1"
        ></meta>
      </Head>
      {showHeaderFooter && <Navbar />}
      <div
        id={"layoutContainer"}
        onContextMenu={() => {
          return false;
        }}
      >
        {children}
      </div>
        {showHeaderFooter && <Footer />}
        <CustomAnimation/>

    </>
  );
};

export default Layout;
