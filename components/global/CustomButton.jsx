import React from "react";

const CustomButton = ({
  color,
  text,
  onClick,
  maxWidth,
  backgroundColor,
  padding,
  disabled,
  className,
  icon,
}) => {
  return (
    <button
      disabled={disabled}
      className={`CustomButton ${!disabled && "btnHoverEffectOutline"} ${
        className ? className : " "
      }`}
      onClick={onClick}
      style={{
        color: color,
        backgroundColor: backgroundColor,
        maxWidth: maxWidth,
        padding: padding,
      }}
    >
      <h6>{text}</h6>
      {icon &&  <div className="btnIcon">{icon}</div>}
     
    </button>
  );
};

export default CustomButton;
