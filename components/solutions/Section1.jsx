import Image from "next/image";
import CustomButton from "@/components/global/CustomButton";
function Section1() {
  return (
    <section className="section1 sectionStyling">
      <div className="section1Inner MaxSize">
        <div className="content">
          <div className="txtBox">
            <h2>Corporate Finance</h2>
            <h4>
              Eget morbi nunc faucibus tellus hendrerit feugiat maecenas feugiat
              mattis. aliquam diam semper est enim vulputate. Viverra augamet, a
              quam porttitor sit eu.
            </h4>

            <CustomButton
              text="Explore"
              backgroundColor="#012A38"
              maxWidth="130px"
              icon={
                <Image
                  width={27}
                  height={12}
                  src="/assets/images/buttonArrow.png"
                  alt={"arrow icon"}
                />
              }
            />
          </div>
          <div className="imgBox">
            <Image
              layout="responsive"
              objectFit="contain"
              width={526}
              height={526}
              src="/assets/images/section1Img.png"
              alt={"3D art"}
            />
          </div>
        </div>
      </div>
    </section>
  );
}

export default Section1;
