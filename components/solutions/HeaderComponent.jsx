
function HeaderComponent() {
  return (
    <header>
      <div className="headerInner">
        <div className="headerBox">
          <div className="content">
            <h6 className="breadCrumbs">Home / Solutions</h6>
            <div className="title">
              <h1>
                <p>S</p>
                <p>o</p>
                <p>l</p>
                <p>u</p>
                <p>t</p>
                <p>i</p>
                <p>o</p>
                <p>n</p>
                <p>s</p>
                {/* Solutions */}
                </h1>
              <p className="subTitle">
              Aliquet mauris est nascetur viverra. Rhoncus tellus quis felis ac
              venenatis urna elit eget.
            </p>
            </div>           
          </div>
        </div>
      </div>
    </header>
  );
}

export default HeaderComponent;
