import Image from "next/image";
import CustomButton from "@/components/global/CustomButton";

function Section2() {
  return (
    <div className="section sectionStyling section2">
      <div className="sectionInner MaxSize">
        <div className="content">
          <div className="imgBox">
            <Image
              layout="responsive"
              objectFit="contain"
              width={"100%"}
              height={"100%"}
              src="/assets/images/section2_1.png"
              alt={"3D art"}
            />
          </div>
          <div className="txtBox">
            <h2>Primary Issuance</h2>
            <h4>
              Eget morbi nunc faucibus tellus hendrerit feugiat maecenas feugiat
              mattis. aliquam diam semper est enim vulputate. Viverra augamet, a
              quam porttitor sit eu.
            </h4>

            <CustomButton
              text="Explore"
              backgroundColor="#012A38"
              maxWidth="130px"
              icon={
                <Image
                  width={27}
                  height={12}
                  src="/assets/images/buttonArrow.png"
                  alt={"arrow icon"}
                />
              }
            />
          </div>
        </div>
        <div className="content">
          <div className="txtBox">
            <h2>Custody</h2>
            <h4>
              Eget morbi nunc faucibus tellus hendrerit feugiat maecenas feugiat
              mattis. aliquam diam semper est enim vulputate. Viverra augamet, a
              quam porttitor sit eu.
            </h4>

            <CustomButton
              text="Explore"
              backgroundColor="#012A38"
              maxWidth="130px"
              icon={
                <Image
                  width={27}
                  height={12}
                  src="/assets/images/buttonArrow.png"
                  alt={"arrow icon"}
                />
              }
            />
          </div>
          <div className="imgBox">
            <Image
              layout="responsive"
              objectFit="contain"
              width={"100%"}
              height={"100%"}
              src="/assets/images/section2_2.png"
              alt={"3D art"}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Section2;
